function [Error, Max_Error, MSE] = calc_error(hspice_data, result)

%hspice_data: the data of waveform file(*.lis) from hspice
%result: the output data simulated by matlab
%Error: the absolute error
%Max_Error: the maximum error (absolute)
%MSE: the mean square error

data_size = size(hspice_data);
Error = zeros(data_size(1), data_size(2));

%hspice data time points
Error(:,1) = hspice_data(:,1);

%linear interpolation of simulated result to compare with hspice data
Error(:,2:end) = interp1(result(:,1), result(:,2:end), Error(:,1), 'linear', 'extrap');

%the absolute error
Error(:,2:end) = abs(Error(:,2:end) - hspice_data(:,2:end));

%the maximum absolute error
Max_Error = max(Error(:,2:end));

%the mean square error
MSE = sqrt(sum(Error(:,2:end).^2)./(data_size(1)-1));