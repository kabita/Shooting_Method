clear;
clc;

%choose one Nerlist file to run
disp( '***Please choose one Netlist!***' );
disp( '1 ---> RLC_s3.sp' );
disp( '2 ---> bus8bit8seg.sp' );
disp( '3 ---> bus32seg16.sp' );
test_file = input( '\n' );

fprintf( '\n' );
%load matrixs from stamp
switch(test_file)
    case 1
        load 'data_sample_RLC.mat';
        load 'hspice_data_RLC.mat';
        result_filename = 'result_RLC_s3';
    case 2
        load 'data_sample_bus8.mat';
        load 'hspice_data_bus8.mat';
        result_filename = 'result_bus8bit8seg';
    case 3
        load 'data_sample_bus32.mat';
        load 'hspice_data_bus32.mat';
        result_filename = 'result_bus32seg16';
    otherwise
        error('Wrong input argument');
end

%T_start: the start of simulate time
%T_end: the end of simulate time
%N: the numbers of iteration
T_start = 0;
T_end = hspice_data(size(hspice_data,1),1);
T_step = hspice_data(2,1) - hspice_data(1,1);
N = floor(T_end / T_step);

%cycle: the cycle number of the waveform
%error_limited: the limited error to stop iteration
%iter_max: the maximum steps of Newton_method to stop iteration
cycle = 5;
error_limited = 1e-9;
iter_max = 50;

%begin to iterate
tic;

[ X_T, Xerror, Herror, index ] = Newton( C, G, B, LT, SRC, T_start, T_end / cycle, floor(N / cycle), error_limited, iter_max );
[results, src_value, ~] = Back_Euler(C, G, B, LT, SRC, T_start, T_end, N, X_T);

toc;

%compute the error
[error , max_error ,MSE] = calc_error(hspice_data,results);

%store data
result_file = strcat(result_filename,'.mat');
save( result_file, 'hspice_data','results','src_value','N','error','max_error','MSE','index','X_T','Xerror','Herror','-mat');

%display the figures and print results
show_dist_Back_Euler( result_file );
