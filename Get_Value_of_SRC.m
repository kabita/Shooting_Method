function src_value = Get_Value_of_SRC(SRC, tn)

% SRC: the matrixs from stamp
% tn: the current time of sampling point
% src_value: the value of input source at sampling point time

src_num = length(SRC);
src_value = zeros(src_num, 1);
u0 = 0;

for i = 1:1:src_num
    %pulse source
    if strcmp(SRC{i,1}{1,1},'pulse')
        para_num = length(SRC{i,1});
        Tstep = 0.001;  %default time step
        v1 = SRC{i,1}{2,1};
        v2 = SRC{i,1}{3,1};
        if para_num > 3
            td = SRC{i,1}{4,1};
        else
            td = 0;
        end
        if para_num > 4
            tr = SRC{i,1}{5,1};
        else
            tr = Tstep;
        end
        if para_num > 5
            tf = SRC{i,1}{6,1};
        else
            tf = Tstep;
        end
        if para_num > 6
            pw = SRC{i,1}{7,1};
        else
            pw = Tstep;
        end
        if para_num > 7
            per = SRC{i,1}{8,1};
        else
            per = Tstep;
        end
        
        t0 = rem(tn, per);

        %judge the voltage of time t0
        if t0 <= td
            u0 = v1;
        elseif t0 > td && t0 <= (td + tr)
            u0 = (v2 - v1)*(t0 - td)/tr + v1;
        elseif t0 > (td + tr) && t0 <= td + tr + pw
            u0 = v2;
        elseif t0 > td + tr + pw && t0 <= td + tr + pw + tf
            u0 = (v2 - v1)*(td + tr + pw + tf - t0)/tf + v1;
        else
            u0 = v1;
        end
    
    %sin source
    elseif strcmp(SRC{i,1}{1,1},'sin')
        para_num = length(SRC{i,1});
        Tstop = 0.1;% default time step
        v0 = SRC{i,1}{2,1};
        va = SRC{i,1}{3,1};
        if para_num > 3
            freq = SRC{i,1}{4,1};
        else
            freq = 1/Tstop;
        end
        if para_num > 4
            tds = SRC{i,1}{5,1};
        else
            tds = 0;
        end
        if para_num > 5
            theta = SRC{i,1}{6,1};
        else
            theta = 0;
        end
        if para_num > 6
            phi = SRC{i,1}{7,1};
        else
            phi = 0;
        end
        
        per = 1/freq;
        ts = rem(tn,per);
        
        %judge the voltage of time ts
        if ts <= tds
            u0 = v0 + va*sin(2*pi*phi/360);
        elseif ts > tds
            u0 = v0 + va*exp(-1*(ts - tds)*theta)*sin(2*pi*(freq*(ts - tds) + phi/360));
        end
        
    else
    end %end of if SRC
    src_value(i) = u0;
    u0 = 0;
end
